import subprocess
from seleniumwire import webdriver
import time

PORT = '3005'
ADDRESS = 'localhost'


if __name__ == '__main__':

    with subprocess.Popen(['python', '-m', 'http.server', PORT, '--bind', ADDRESS], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
        print('serving at port', PORT)

        options = {
            'proxy': {
                'http': 'http://10.10.10.10:8080/',
                'https': 'http://10.10.10.10:8080/',
                'no_proxy': 'localhost,127.0.0.1'
            },
        }
        browser = webdriver.Chrome(seleniumwire_options=options)
        browser.minimize_window()
        time.sleep(2)

        browser.get(f'http://localhost:{PORT}')


        pause = input('\n\npress enter to quit\n\n')
        browser.quit()
        proc.kill()
